import UIKit

struct Profile {
    var name, email, password : String
}

class User {
    var profile = Profile(name: "Sergey", email: "test@tes.ru", password: "123")
    
    func auth(email: String, password: String) -> String {
        if self.profile.email == email && self.profile.password == password {
            return "Пользватель авторизован"
        } else {
            return "Пользватель не авторизован"
        }
    }
    
    func register(name: String, email: String, password: String, password2: String) -> String {
        if password == password2 {
            profile = Profile(name: name, email: email, password: password)
            return "Пользователь зарегистрирован"
        } else {
            return "Пароли не совпадают"
        }
    }
}

let user = User()
print(user.register(name: "Petya", email: "test2@test.ru", password: "321", password2: "123"))
print(user.auth(email: "test2@test.ru", password: "321"))





//print(user.check(password: user.profile.password))

//print(User().check(password: User().profile.password))
